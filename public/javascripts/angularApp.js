var module = angular.module('east-west', ['ui.bootstrap']);

module.controller('eastSide', ['$scope', function($scope){
	$scope.myInterval = 3000;
	$scope.eastPlayers = [
		{name: 'D Marcus Williums', image: '/images/default.jpg'},
		{name: 'T.J. Juckson', image: '/images/default.jpg'},
		{name: 'T varisuness King', image: '/images/default.jpg'},
		{name: 'Tyroil Smoochie-Wallace', image: '/images/default.jpg'},
		{name: 'D Squarius Green, Jr.', image: '/images/default.jpg'},
		{name: 'Ibrahim Moizoos', image: '/images/default.jpg'},
		{name: 'Jackmerius Tacktheritrix', image: '/images/default.jpg'},
		{name: 'D Isiah T. Billings-Clyde', image: '/images/default.jpg'},
		{name: 'D Jasper Probincrux III', image: '/images/default.jpg'},
		{name: 'Leoz Maxwell Jilliumz', image: '/images/default.jpg'},
		{name: 'Javaris Jamar Javarison-Lamar', image: '/images/default.jpg'},
		{name: 'Davoin Shower-Handel', image: '/images/default.jpg'},
		{name: 'Hingle McCringleberry', image: '/images/default.jpg'},
		{name: 'L Carpetron Dookmarriot', image: '/images/default.jpg'},
		{name: 'J Dinkalage Morgoone', image: '/images/default.jpg'},
		{name: 'Xmus Jaxon Flaxon-Waxon', image: '/images/default.jpg'},
		{name: 'Coznesster Smiff', image: '/images/default.jpg'},
		{name: 'Elipses Corter', image: '/images/default.jpg'},
		{name: 'Nyquillus Dillwad', image: '/images/default.jpg'},
		{name: 'Bismo Funyuns', image: '/images/default.jpg'},
		{name: 'Decatholac Mango', image: '/images/default.jpg'},
		{name: 'Mergatroid Skittle', image: '/images/default.jpg'},
		{name: 'Quiznatodd Bidness', image: '/images/default.jpg'},
		{name: 'D Pez Poopsie', image: '/images/default.jpg'},
		{name: 'Quackadilly Blip', image: '/images/default.jpg'},
		{name: 'Goolius Boozler', image: '/images/default.jpg'},
		{name: 'Bisquiteen Trisket', image: '/images/default.jpg'},
		{name: 'Fartrell Cluggins', image: '/images/default.jpg'},
		{name: 'Blyrone Blashinton',image: '/images/default.jpg'},
		{name: 'Cartoons Plural', image: '/images/default.jpg'},
		{name: 'Jammie Jammie-Jammie', image: '/images/default.jpg'},
		{name: 'Fudge', image: '/images/default.jpg'}
	];
}]);

module.controller('westSide', ['$scope', function($scope){
	$scope.myInterval = 3000;
	$scope.westPlayers = [
		{name: 'Saggitariutt Jefferspin', image: '/images/default.jpg'},
		{name: 'D Glester Hardunkichud', image: '/images/default.jpg'},
		{name: 'Swirvithan L. Goodling-Splatt', image: '/images/default.jpg'},
		{name: 'Quatro Quatro', image: '/images/default.jpg'},
		{name: 'Ozamataz Buckshank', image: '/images/default.jpg'},
		{name: 'Beezer Twelve Washingbeard', image: '/images/default.jpg'},
		{name: 'Shakiraquan T.G.I.F. Carter', image: '/images/default.jpg'},
		{name: 'X-Wing @Aliciousness', image: '/images/default.jpg'},
		{name: 'Sequester Grundelplith M.D.', image: '/images/default.jpg'},
		{name: 'Scoish Velociraptor Maloish', image: '/images/default.jpg'},
		{name: 'T.J. A.J. R.J. Backslashinfourth V', image: '/images/default.jpg'},
		{name: 'Eeeee Eeeeeeeee', image: '/images/default.jpg'},
		{name: 'Donkey Teeth', image: '/images/default.jpg'},
		{name: 'Torque (Construction Noise) Lewith', image: '/images/default.jpg'},
		{name: 'The Player Formerly Known as Mousecop', image: '/images/default.jpg'},
		{name: 'Dan Smith', image: '/images/default.jpg'},
		{name: 'Equine Ducklings', image: '/images/default.jpg'},
		{name: 'Dahistorius Lamystorius', image: '/images/default.jpg'},
		{name: 'Ewokoniad Sigourneth JuniorStein', image: '/images/default.jpg'},
		{name: 'Eqqsnuizitine Buble-Schwinslow', image: '/images/default.jpg'},
		{name: 'Huka lakanaka Hakanakaheekalucka hukahakafaka', image: '/images/default.jpg'},
		{name: 'King Prince Chambermaid', image: '/images/default.jpg'},
		{name: 'Ladennifer Jadaniston', image: '/images/default.jpg'},
		{name: 'Ladadadaladadadadada Dala-Dadaladaladalada', image: '/images/default.jpg'},
		{name: 'Harvard University', image: '/images/default.jpg'},
		{name: 'Morse Code', image: '/images/default.jpg'},
		{name: 'Wingdings', image: '/images/default.jpg'},
		{name: 'Firstname Lastname', image: '/images/default.jpg'},
		{name: 'God', image: '/images/default.jpg'},
		{name: 'Squeeeeeeeeeeps', image: '/images/default.jpg'},
		{name: 'Benedict Cumberbatch', image: '/images/default.jpg'},
		{name: 'A.A. Ron Balakay', image: '/images/default.jpg'}
	];
}]);